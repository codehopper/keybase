## Copyright 2019 Code Hopper -- https://codehopper.nl
##
## Permission is hereby granted, free of charge, to any person obtaining a copy of
## this software and associated documentation files (the "Software"), to deal in the
## Software without restriction, including without limitation the rights to use,
## copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
## Software, and to permit persons to whom the Software is furnished to do so, subject
## to the following conditions:
##
## The above copyright notice and this permission notice shall be included in all copies
## or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLI
## ED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
## A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
## ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
## THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
##
##                      d8b         d8b
##                      88P         ?88
##                     d88           88b
##  d8888b d8888b  d888888   d8888b  888888b  d8888b ?88,.d88b,?88,.d88b, d8888b  88bd88b
## d8P' `Pd8P' ?88d8P' ?88  d8b_,dP  88P `?8bd8P' ?88`?88'  ?88`?88'  ?88d8b_,dP  88P'  `
## 88b    88b  d8888b  ,88b 88b     d88   88P88b  d88  88b  d8P  88b  d8P88b     d88
## `?888P'`?8888P'`?88P'`88b`?888P'd88'   88b`?8888P'  888888P'  888888P'`?888P'd88'
##                                                     88P'      88P'
##                                                    d88       d88
##                                                    ?8P       ?8P
##
## Software and technology done right
## https://codehopper.nl
##
## Keybase client Docker image Makefile
##
#########################################################################################

conf ?= config.env

include $(conf)                        # import variables from config
export $(shell sed 's/=.*//' $(conf))  # export the imported vars

.PHONY: help
.DEFAULT_GOAL := help

# Curtesy of https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db
help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

build: ## Build the container
	docker build -t $(IMAGE_NAME) .

build-nc: ## Build the container without caching
	docker build --no-cache -t $(IMAGE_NAME) .

release: build-nc publish ## Make a release by building and publishing the `{version}` and `latest` tagged containers to Docker Hub

publish: repo-login publish-latest publish-version ## Publish the `{version}` and `latest` tagged containers to Docker Hub

publish-latest: tag-latest ## Publish the `latest` taged container to Docker Hub
	@echo 'publish latest to Docker Hub'
	docker push $(IMAGE_NAME):latest

publish-version: tag-version ## Publish the `{version}` taged container to Docker Hub
	@echo 'publish $(VERSION) to Docker Hub'
	docker push $(IMAGE_NAME):$(VERSION)

# Docker tagging
tag: tag-latest tag-version ## Generate container tags for the `{version}` and `latest` tags

tag-latest: ## Generate container `{version}` tag
	@echo 'create tag latest'
	docker tag $(IMAGE_NAME) $(IMAGE_NAME):latest

tag-version: ## Generate container `latest` tag
	@echo 'create tag $(VERSION)'
	docker tag $(IMAGE_NAME) $(IMAGE_NAME):$(VERSION)

repo-login: ## Login to Docker Hub
	docker login

version: ## Outputs the current version
	@echo version
