## Copyright 2019 Code Hopper -- https://codehopper.nl
##
## Permission is hereby granted, free of charge, to any person obtaining a copy of
## this software and associated documentation files (the "Software"), to deal in the
## Software without restriction, including without limitation the rights to use,
## copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
## Software, and to permit persons to whom the Software is furnished to do so, subject
## to the following conditions:
##
## The above copyright notice and this permission notice shall be included in all copies
## or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLI
## ED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
## A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
## ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
## THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
##
##                      d8b         d8b
##                      88P         ?88
##                     d88           88b
##  d8888b d8888b  d888888   d8888b  888888b  d8888b ?88,.d88b,?88,.d88b, d8888b  88bd88b
## d8P' `Pd8P' ?88d8P' ?88  d8b_,dP  88P `?8bd8P' ?88`?88'  ?88`?88'  ?88d8b_,dP  88P'  `
## 88b    88b  d8888b  ,88b 88b     d88   88P88b  d88  88b  d8P  88b  d8P88b     d88
## `?888P'`?8888P'`?88P'`88b`?888P'd88'   88b`?8888P'  888888P'  888888P'`?888P'd88'
##                                                     88P'      88P'
##                                                    d88       d88
##                                                    ?8P       ?8P
##
## Software and technology done right
## https://codehopper.nl
##
## Keybase client Docker image
##
#########################################################################################

FROM debian:buster-slim
MAINTAINER info@codehopper.nl

WORKDIR /tmp

# Get dependencies
RUN apt-get update; \
    apt-get install --assume-yes curl

# Install keybase as per https://keybase.io/docs/the_app/install_linux
RUN curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
RUN dpkg -i keybase_amd64.deb || apt-get install --assume-yes -f

# Cleanup
RUN rm keybase_amd64.deb

# Allow root Login
RUN echo 'root:root' | chpasswd

# Copy the entrypoint
COPY entrypoint.sh /usr/local/bin/

# Keybase requires a non-root user
RUN useradd -ms /bin/bash keybase
USER keybase
WORKDIR /home/keybase

# Start keybase and start a shell
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["/bin/bash"]
